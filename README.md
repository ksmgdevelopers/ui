# KSMG ui library

### Live on [https://ui.ksmg.now.sh/](https://ui.ksmg.now.sh/)

Tools:

[Storybook docs](https://storybook.js.org/) - Documentation and live preview

[Vue cli](https://cli.vuejs.org/) - Bundling

[Vercel](https://vercel.com/) - Serving static files

## Using in project

Install with package manager
```
npm i @ksmg/ui -S
``` 
or 
```
yarn add @ksmg/ui
```

Use just styles from `@ksmg/ui/dist/ui.css`


You can use library as Vue plugin to import all components at once
```
import Vue from 'vue'
import '@ksmg/ui/dist/ui.css'
import ui from '@ksmg/ui'

Vue.use(ui)
```

Or import component on demand. You still need to import css bundle
```
<template>
    <k-rounded-button>Submit</k-rounded-button>
</template>

<script>
import { KRoundedButton } from '@ksmg/ui'

expord default {
    components: { KRoundedButton }
}
</script>
```

Or import component from sources with styles (vue-loader needed)
```
<template>
    <k-rounded-button>Submit</k-rounded-button>
</template>

<script>
import { KRoundedButton } from '@ksmg/ui/src/componentst/KRoundedButton'

expord default {
    components: { KRoundedButton }
}
</script>
```

## Project development
Install dependencies
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles storybook for production
```
npm run build
```

### Compiles and minifies for publishing
```
npm run build:bundle
```

### Change version and publish to npm
```
npm publish
```

### Serve storybook for development
```
npm run storybook:serve
```

Write storybook stories in `src/stories` folder

## Publishing storybook to now.sh
Install now
```
npm -g vercel
```

Login through your email
```
vercel login
```

Publish. You might need to be added to KSMG team
```
vercel --prod
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
