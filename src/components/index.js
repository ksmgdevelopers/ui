import '../styles/index.scss'

import KRoundedButton from './KRoundedButton'
import KTabButton from './KTabButton'
import KTabItem from './KTabItem'
import KRadio from './KRadio'
import KAvatar from './KAvatar'
import KInput from './KInput'
import KCheckbox from './KCheckbox'
import KSwitch from './KSwitch'
import KCard from './KCard'
import KField from './KField'
import KSelect from './KSelect'
import KLineTrailer from './KLineTrailer'
import KDatepicker from './KDatepicker'
import KButton from './KButton'
import KFile from './KFile'
import KModal from './KModal'
import KRating from './KRating'
import KImageFile from './KImageFile'
import KSpinner from './KSpinner'
import KProgressBar from './KProgressBar'
import KHeader from './layout/header/KHeader'
import KSubheader from './layout/header/KSubheader'
import KSubmitter from './KSubmitter'
import KIcon from './KIcon'
import KTextarea from './KTextarea'
import KBadge from './KBadge'
import KDropMenu from './KDropMenu'
import KDropMenuContent from './KDropMenuContent'
import KDropMenuItem from './KDropMenuItem'
import KRoundedInput from './KRoundedInput'
import KPagination from './KPagination'
import KSkeleton from './KSkeleton'

const Components = {
  KButton,
  KRoundedButton,
  KRadio,
  KAvatar,
  KInput,
  KTextarea,
  KFile,
  KImageFile,
  KCheckbox,
  KSwitch,
  KSelect,
  KField,
  KCard,
  KLineTrailer,
  KDatepicker,
  KTabButton,
  KTabItem,
  KModal,
  KRating,
  KSpinner,
  KProgressBar,
  KHeader,
  KSubheader,
  KSubmitter,
  KIcon,
  KBadge,
  KDropMenu,
  KDropMenuContent,
  KDropMenuItem,
  KRoundedInput,
  KPagination,
  KSkeleton,
}

export default {
  install(Vue) {
    Object.keys(Components).forEach(name => {
      Vue.component(name, Components[name])
    })
    // TODO: pass options argument is an array of components to be registered e.g. ['k-button', 'k-modal']
    // if (typeof options === 'undefined') {
    // Register all components
    // }
    // else {
    //   if (!(options instanceof Array)) {
    //     throw new TypeError('options must be an array');
    //   }
    //
    //   // eslint-disable-next-line no-console
    //   console.log('here')
    //   for (let c of Components) {
    //     // register only components specified in the options
    //     if (options.includes(c.default.name)) {
    //       Vue.component(c.default.name, c.default);
    //     }
    //   }
    // }
  },
}

// export standalone components
export {
  KRoundedButton,
  KTabButton,
  KButton,
  KTabItem,
  KRadio,
  KFile,
  KImageFile,
  KAvatar,
  KInput,
  KTextarea,
  KCheckbox,
  KSwitch,
  KSelect,
  KField,
  KLineTrailer,
  KCard,
  KDatepicker,
  KModal,
  KRating,
  KSpinner,
  KProgressBar,
  KHeader,
  KSubheader,
  KSubmitter,
  KIcon,
  KBadge,
  KDropMenu,
  KDropMenuItem,
  KDropMenuContent,
  KRoundedInput,
  KPagination,
  KSkeleton,
}
