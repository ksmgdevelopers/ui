import generateId from '../core/utils/generateId'

export default {
  props: {
    id: { type: String },
    value: {},
    placeholder: String,
    name: String,
    maxlength: [String, Number],
    readonly: Boolean,
    required: Boolean,
    disabled: Boolean,
    kCounter: [String, Number],
  },
  data() {
    return {
      localValue: this.value,
      textareaHeight: false,
    }
  },
  computed: {
    model: {
      get() {
        return this.localValue
      },
      set(value) {
        if (value.constructor.toString().match(/function (\w*)/)[1].toLowerCase() !== 'inputevent') {
          this.$nextTick(() => {
            this.localValue = value
          })
        }
      },
    },
    clear() {
      return this.KField.clear
    },
    attributes() {
      return {
        ...this.$attrs,
        type: this.type,
        id: this.id,
        name: this.name,
        disabled: this.disabled,
        required: this.required,
        placeholder: this.placeholder,
        readonly: this.readonly,
        maxlength: this.maxlength,
      }
    },
  },
  watch: {
    model() {
      this.setFieldValue()
    },
    clear(clear) {
      if (clear) {
        this.clearField()
      }
    },
    placeholder() {
      this.setPlaceholder()
    },
    disabled() {
      this.setDisabled()
    },
    required() {
      this.setRequired()
    },
    maxlength() {
      this.setMaxlength()
    },
    kCounter() {
      this.setMaxlength()
    },
    localValue(val) {
      this.$emit('input', val)
    },
    value(val) {
      this.localValue = val
    },
    id() {
      this.setId()
    },
  },
  methods: {
    clearField() {
      this.$el.value = ''
      this.model = ''
      this.setFieldValue()
    },
    setLabelFor() {
      if (!this.$el.parentNode) {
        return
      }

      const label = this.$el.parentNode.querySelector('label')

      if (!label) {
        return
      }

      const forAttribute = label.getAttribute('for')

      if (!forAttribute) {
        label.setAttribute('for', this.KField.id)
      }
    },
    setFieldValue() {
      this.KField.value = this.model
    },
    setPlaceholder() {
      this.KField.placeholder = Boolean(this.placeholder)
    },
    setDisabled() {
      this.KField.disabled = Boolean(this.disabled)
    },
    setRequired() {
      this.KField.required = Boolean(this.required)
    },
    setId() {
      if (!this.id) {
        this.KField.id  = generateId()
      } else {
        this.KField.id = this.id
      }
    },
    setMaxlength() {
      if (this.kCounter) {
        this.KField.counter = parseInt(this.kCounter, 10)
      } else {
        this.KField.maxlength = parseInt(this.maxlength, 10)
      }
    },
    onFocus() {
      this.KField.focused = true
    },
    onBlur() {
      this.KField.focused = false
    },
  },
  created() {
    this.setFieldValue()
    this.setPlaceholder()
    this.setDisabled()
    this.setRequired()
    this.setMaxlength()
    this.setId()
  },
  mounted() {
    this.setLabelFor()
  },
}
