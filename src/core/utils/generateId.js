const idCounter = {}

/**
 * Generate unique id for inputs and labels
 * @param {String} prefix
 * @returns {string}
 */
function generateId(prefix = 'k-id-') {
  if (!idCounter[prefix]) {
    idCounter[prefix] = 0
  }

  const id = ++idCounter[prefix]
  return `${prefix}${id}`
}

export default generateId
