/**
 * @param {File[]} arrayOfFiles
 * @returns {FileList}
 * @constructor
 */
export default function fileListFromArray(arrayOfFiles) {
  arrayOfFiles = [].slice.call(Array.isArray(arrayOfFiles) ? arrayOfFiles : arguments);
  let c, container, d;
  for (c, container = c = arrayOfFiles.length, d = !0; container-- && d;) {
    d = arrayOfFiles[container] instanceof File;
  }
  if (!d) {
    throw new TypeError('expected argument to FileList is File or array of File objects');
  }
  for (container = (new ClipboardEvent('')).clipboardData || new DataTransfer; c--;) {
    container.items.add(arrayOfFiles[c]);
  }
  return container.files
}
