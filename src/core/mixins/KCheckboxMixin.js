export default {
  props: {
    label: String,
    name: String,
    model: [String, Boolean, Object, Number, Array],
    value: {
      type: [String, Boolean, Object, Number],
      default: 'on',
    },
    required: { type: Boolean, default: false },
    forceChecked: { type: Boolean, default: undefined },
    disabled: { type: Boolean, default: false },
    trueValue: { default: true },
    falseValue: { default: false },
  },
  model: {
    prop: 'model',
    event: 'change',
  },
  methods: {
    toggleCheck() {
      if (this.disabled || this.forceChecked !== undefined) {
        return
      }

      if (this.isModelArray) {
        this.handleArrayCheckbox()
      } else if (this.hasValue) {
        this.handleSingleSelectCheckbox()
      } else {
        this.handleSimpleCheckbox()
      }
    },
    handleSimpleCheckbox() {
      this.$emit('change', this.isSelected ? this.falseValue : this.trueValue)
    },
    handleSingleSelectCheckbox() {
      this.$emit('change', this.isSelected ? null : this.value)
    },
    handleArrayCheckbox() {
      const newModel = this.model

      if (!this.isSelected) {
        newModel.push(this.value)
      } else {
        this.removeItemFromModel(newModel)
      }

      this.$emit('change', newModel)
    },
    removeItemFromModel(newModel) {
      const index = newModel.indexOf(this.value)

      if (index !== -1) {
        newModel.splice(index, 1)
      }
    },
  },
  computed: {
    attrs() {
      const attrs = {
        ...this.$attrs,
        id: this.id,
        name: this.name,
        disabled: this.disabled,
        required: this.required,
        checked: this.isSelected,
        'true-value': this.trueValue,
        'false-value': this.falseValue,
      }

      if (this.$options.propsData.hasOwnProperty('value')) {
        if (this.value === null || typeof this.value !== 'object') {
          attrs.value = (this.value === null || this.value === undefined) ? '' : String(this.value)
        }
      }

      return attrs
    },
    hasValue() {
      return this.$options.propsData.hasOwnProperty('value')
    },
    isSelected() {
      if (this.forceChecked !== undefined) {
        return this.forceChecked
      }

      if (this.$attrs.checked !== undefined) {
        return this.$attrs.checked
      }

      if (this.isModelArray) {
        return this.model.includes(this.value)
      }

      if (this.hasValue) {
        return this.model === this.value
      }

      return this.model === this.trueValue
    },
    isModelArray() {
      return Array.isArray(this.model)
    },
  },
}
