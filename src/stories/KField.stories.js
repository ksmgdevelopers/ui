import { storiesOf } from '@storybook/vue'
import KInput from '../components/KInput'
import KField from '../components/KField'
import { withKnobs, boolean, text } from '@storybook/addon-knobs'
import { action } from '@storybook/addon-actions'

storiesOf('Components/Form field', module)
  .addDecorator(withKnobs)
  .add('Input',
    () => ({
      data() {
        return {
          inputValue: text('Input value', 'John'),
          input: action('change input'),
          input2Value: 'Errored',
          invalid: boolean('Invalid input', true),
          error1: boolean('Error 1', true),
          error2: boolean('Error 2', true),
          error3: boolean('Error 3', true),
          number: 0,
        }
      },
      components: { KField, KInput },
      template: `
        <div>
          <k-field>
            <k-input id="input" v-model="inputValue" @input="input" disabled/>
            <label class="k-field-label" for="input">Name</label>
            <span class="k-field-helper" slot="helper">Helper text</span>
          </k-field>

          <k-field purple>
            <k-input placeholder="Purple"/>
            <label class="k-field-label">Name</label>
            <span class="k-field-helper" slot="helper">Helper text</span>
          </k-field>

          <k-field :class="{'k-field--invalid': invalid}">
            <k-input :value="input2Value" type="email"/>
            <label class="k-field-label">Email</label>
            <span class="k-field-helper">Helper text</span>
            <div slot="helper">
              <span class="k-field-error" v-if="error1">Error text 1</span>
              <span class="k-field-error" v-if="error2">Error text 2</span>
              <span class="k-field-error" v-if="error3">Error text 3</span>
            </div>
          </k-field>

          <k-field>
            <k-input :value="input2Value" k-counter="30"/>
            <label class="k-field-label">Char counter</label>
            <span class="k-field-helper" slot="helper">Chars chars chars</span>
          </k-field>

          <k-field>
            <k-input :value="'Max length. Try to write more'" maxlength="30"/>
            <label class="k-field-label">Max length</label>
          </k-field>
          <k-field>
            <k-input :value="number" type="number"/>
            <label class="k-field-label">Number</label>
          </k-field>

          No padding
          <k-field no-padding>
            <k-input/>
          </k-field>
        </div>
      `,
    }),
    {
      info: {
        summary: 'Field',
      },
    },
  )
