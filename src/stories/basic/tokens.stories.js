import { addParameters, storiesOf } from '@storybook/vue'
import capitalize from '../../filters/capitalize'

const { default: colorsString } = require('!!raw-loader!../../styles/tokens/colors.scss')
const { default: fontSizesString } = require('!!raw-loader!../../styles/tokens/font-size.scss')
const { default: boxShadowString } = require('!!raw-loader!../../styles/tokens/_box-shadow.scss')
const { default: borderRadiusesString } = require('!!raw-loader!../../styles/tokens/border-radius.scss')
const { default: fontString } = require('!!raw-loader!../../styles/tokens/fonts.scss')


const scssToObj = (acc, fontSizeString) => {
  if (fontSizeString.trim().startsWith('//') || fontSizeString.trim().startsWith('/*') || fontSizeString.trim().startsWith('*')) {
    return acc
  }
  const [name, value] = fontSizeString.split(':')
  acc[name] = (value.slice(0, value.length - 1))
  return acc
}

const tdStyle = {
  padding: '.5rem',
}

const [scssColorVariables] = colorsString.split(':export')
const colors = scssColorVariables.split('\n').filter(Boolean).reduce(scssToObj, {})
const fontSizes = fontSizesString.split('\n').filter(Boolean).reduce(scssToObj, {})
const borderRadiuses = borderRadiusesString.split('\n').filter(Boolean).reduce(scssToObj, {})
const boxShadows = boxShadowString.split('\n').filter(Boolean).reduce(scssToObj, {})
const [fontsVariables] = fontString.split(':export')
const fonts = fontsVariables.split('\n').filter(Boolean).reduce(scssToObj, {})


addParameters({ colors, fontSizes })

storiesOf('Basic/Design tokens', module)
  .add(
    'Box-shadow',
    () => ({
      filters: {
        capitalize,
      },
      data() {
        return { boxShadows, tdStyle }
      },
      template: `
        <div>
        <table>
          <thead>
          <tr>
            <th>Name</th>
            <th>Scss variable</th>
            <th>Preview</th>
          </tr>
          </thead>
          <tbody>
          <tr v-for="(boxShadow, name) in boxShadows">
            <td :style="tdStyle">{{ name.substr(1, name.length) | capitalize }}</td>
            <td :style="tdStyle">{{ name }}</td>
            <td :style="tdStyle">
              <div style="width: 100px; height: 100px; background: white;" :style="{boxShadow}"></div>
            </td>
          </tr>
          </tbody>
        </table>
        </div>
      `,
    }),
  )
  .add(
    'Colors',
    () => ({
      data: () => ({
        colors,
        tdStyle,
      }),
      filters: { capitalize },
      template: `
        <div>
        <h1>Colors</h1>
        <table :style="{width: '100%', 'border-spacing': '4px'}">
          <thead>
          <tr>
            <th :style="tdStyle">Name</th>
            <th :style="tdStyle">Scss variable</th>
            <th :style="tdStyle">Hex</th>
            <th :style="tdStyle"></th>
          </tr>
          </thead>
          <tbody>
          <tr v-for="(color, name) in colors">
            <td :style="tdStyle">{{ name.substr(1, name.length) | capitalize }}</td>
            <td :style="tdStyle">{{ name }}</td>
            <td :style="tdStyle">{{ color }}</td>
            <td :style="{'background-color': color, 'min-width': '150px', ...tdStyle}"></td>
          </tr>
          </tbody>
        </table>
        </div>`,
    }),
  )
  .add(
    'Fonts',
    () => ({
      filters: {
        capitalize,
      },
      data() {
        return { fonts, tdStyle }
      },
      template: `
        <div>
        <h1>Fonts</h1>
        <table>
          <tr v-for="(font, name) in fonts">
            <td :style="tdStyle">{{ name.substr(1, name.length) | capitalize }}</td>
            <td :style="tdStyle">{{ name }}</td>
            <td :style="tdStyle">{{ font }}</td>
            <td :style="{...tdStyle, fontFamily: font, fontSize: '18px'}">The quick brown fox jumps over the lazy
              dog
            </td>
          </tr>
        </table>
        </div>
      `,
    }),
  )
  .add(
    'Typography',
    () => ({
      filters: { capitalize },
      data: () => ({
        fontSizes,
        tdStyle,
      }),
      template: `
        <div>
        <h1>Typography</h1>
        <h2>Font size</h2>
        <table>
          <thead>
          <tr>
            <th>Name</th>
            <th>Sass variable</th>
            <th>Size</th>
            <th>Preview</th>
          </tr>
          </thead>
          <tbody>
          <tr v-for="(size, name) in fontSizes">
            <td :style="tdStyle">{{ name.substr(1, name.length) | capitalize }}</td>
            <td :style="tdStyle">{{ name }}</td>
            <td :style="tdStyle">{{ size }}</td>
            <td :style="{'font-size': size, ...tdStyle}">The quick black fox jumped over the lazy dog</td>
          </tr>
          </tbody>
        </table>

        </div>`,
    }),
  )
  .add(
    'Border-radius',
    () => ({
      filters: { capitalize },
      data() {
        return {
          borderRadiuses,
          tdStyle,
        }
      },
      template: `
        <div>
        <table>
          <thead>
          <tr>
            <th>Name</th>
            <th>Variable</th>
            <th>Value</th>
            <th>Preview</th>
          </tr>
          </thead>
          <tbody>
          <tr v-for="(radius, name) in borderRadiuses">
            <td :style="tdStyle">
              {{ name.substr(1, name.length).split('-').join(' ') | capitalize }}
            </td>
            <td :style="tdStyle">{{ name }}</td>
            <td :style="tdStyle">{{ radius }}</td>
            <td :style="tdStyle">
              <div :style="{width: '40px', height: '40px', 'background-color': '#000', borderRadius: radius}"></div>
            </td>
          </tr>
          </tbody>
        </table>
        </div>
      `,
    }),
  )
  .add(
    'Selection',
    () => ({
      template: `
      <div class="k-theme">
      <h1>Selected text highlights with color</h1>
      <h2>Just add "k-theme" to text block, or to html/body tag for global usage</h2>
      <p>Through relevant content we yield results
Each year we help more than 500 employers to spread their message and deliver results through social media. 9 years ago we established the concept social media recruitment and have since then built a solid expertise in the area. We create target audiences and build talent pools for our clients to fulfil current and future hire demands. Our agile team deliver high quality data-driven advertising including project management and advisory services during the course of work.
</p>
</div>
      `,
    }),
  )

