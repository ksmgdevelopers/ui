import { storiesOf } from '@storybook/vue'
import { withKnobs, boolean } from '@storybook/addon-knobs'
import KSubmitter from '../components/KSubmitter'
import KRoundedButton from '../components/KRoundedButton'

storiesOf('Components/Submitter', module)
  .addDecorator(withKnobs)
  .add(
    'Pretty submit',
    () => ({
      components: { KSubmitter, KRoundedButton },
      data() {
        return {
          loading: boolean('Loading', false),
          error: boolean('Error', false),
          success: boolean('Success', false),
          aloading: false,
          aerror: false,
          asuccess: false,
        }
      },
      mounted() {
        let bools = [true, false, false, false]
        setInterval(() => {
          this.aloading = bools[0]
          this.aerror = bools[1]
          this.asuccess = bools[2]
          bools.unshift(bools.pop())
        }, 2000)
      },
      template: `<div>
  <h3>Customizable loading, error, success state and change delay</h3>

  <h5>You can control it with knobs</h5>
  <k-submitter :loading="loading" :error="error" :success="success">
    <k-rounded-button secondary grouped>Cancel</k-rounded-button>
    <k-rounded-button>Submit</k-rounded-button>
  </k-submitter>

  <h5>Automatic switch</h5>
  <k-submitter :loading="aloading" :error="aerror" :success="asuccess">
    <k-rounded-button secondary grouped>Cancel</k-rounded-button>
    <k-rounded-button>Submit</k-rounded-button>
  </k-submitter>
</div>`,
    }),
    { info: { summary: 'Submiter' } },
  )
