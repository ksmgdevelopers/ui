import { storiesOf } from '@storybook/vue'
import KRoundedInput from '../../components/KRoundedInput'
import KIcon from '../../components/KIcon'

storiesOf('Components/Form field', module)
  .add(
    'Rounded Input',
    () => ({
      components: { KRoundedInput, KIcon },
      data() {
        return {
          model: '',
          ev: '',
        }
      },
      template: `
        <div style="background: #fdf4ec; padding: 20px">
          <h5>Model binding</h5>
          <k-rounded-input v-model="model"/>
          <p></p>
          <k-rounded-input placeholder="Placeholder" v-model="model" purple/>
          <hr>
          <h5>Slots</h5>
          <k-rounded-input value="Init value" placeholder="Placeholder">
            <template slot="left">
              <div style="padding-left: 18px; padding-top: 10px">
                <k-icon name="magnifier"/>
              </div>
            </template>
          </k-rounded-input>
          <p></p>
          <k-rounded-input :value="1" placeholder="Placeholder">
            <template slot="right">
              <div style="padding-right: 18px; padding-top: 10px">
                <k-icon name="magnifier"/>
              </div>
            </template>
          </k-rounded-input>
          <p></p>
          <label for="events">With events</label>
          <k-rounded-input id="events" @focus="ev = 'focus'" @blur="ev = 'blur'" @keydown="ev = 'keydown'"/>
          <pre v-text="ev"/>
        </div>
      `,
    }),
    { info: { summary: 'Rounded input' } },
  )
