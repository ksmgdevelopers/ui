import { storiesOf } from '@storybook/vue'
import KProgressBar from '../../components/KProgressBar'

storiesOf('Components/Progress Bar', module)
  .add(
    'Progress Bar',
    () => ({
      components: { KProgressBar },
      template: `<div>
  <h4 style="margin-bottom: 0;">Thin</h4>
  <k-progress-bar thin></k-progress-bar>
  <h4 style="margin-bottom: 0;">Normal</h4>
  <k-progress-bar></k-progress-bar>
  <h4 style="margin-bottom: 0;">Thick</h4>
  <k-progress-bar thick></k-progress-bar>

  <h4 style="margin-bottom: 0;">Orange</h4>
  <k-progress-bar orange></k-progress-bar>
  <h4 style="margin-bottom: 0;">Red</h4>
  <k-progress-bar red></k-progress-bar>
</div>
        `,
    }),
    { info: { summary: 'Progress bar' } },
  )
