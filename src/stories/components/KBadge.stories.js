import { storiesOf } from '@storybook/vue'
import KBadge from '../../components/KBadge'
import KButton from '../../components/KButton'

storiesOf('Components/Badge', module)
  .add(
    'Badges',
    () => ({
      components: { KBadge, KButton },
      data() {
        return { changing: 0 }
      },
      created() {
        setInterval(() => {
          this.changing = Number(!this.changing)
        }, 2500)
      },
      template: `
        <div>
          <k-badge position="top left" type="danger" style="margin-right: 45px">
            <k-button>Button</k-button>
          </k-badge>
          <k-badge :content="45" position="top right" type="warning" style="margin-right: 45px">
            <k-button>Button</k-button>
          </k-badge>
          <k-badge :content="12" position="bottom left" type="success" style="margin-right: 45px">
            <k-button>Button</k-button>
          </k-badge>
          <k-badge :content="999" position="bottom right" style="margin-right: 45px">
            <k-button>BIG</k-button>
          </k-badge>
          <k-badge :content="changing" hide-on-empty position="bottom left" style="margin-right: 45px">
            <k-button>Hide empty {{changing}}</k-button>
          </k-badge>
        </div>
      `,
    }),
    {
      info: {
        summary:
          'Badge',
      },
    },
  )
