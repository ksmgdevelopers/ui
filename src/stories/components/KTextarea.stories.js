import { storiesOf } from '@storybook/vue'
import KTextarea from '../../components/KTextarea'
import KField from '../../components/KField'
import { withKnobs } from '@storybook/addon-knobs'

storiesOf('Components/Form field', module)
  .addDecorator(withKnobs)
  .add(
    'Textarea',
    () => ({
      components: { KTextarea, KField },
      data() {
        return {
          first: '',
          disabled: 'Hi!',
          maxlength: '',
        }
      },
      template: `
        <div>
          <k-field>
            <label class="k-field-label">Textarea</label>
            <k-textarea v-model="first" required placeholder="Holder"/>
            <span class="k-field-helper" slot="helper">Helper text</span>
          </k-field>
          <k-field>
            <label class="k-field-label">Textarea</label>
            <k-textarea v-model="disabled" disabled/>
          </k-field>
          <k-field purple>
            <label class="k-field-label">Textarea purple</label>
            <k-textarea v-model="maxlength" required maxlength="100"/>
          </k-field>
        </div>
      `,
    }),
    {
      info: { summary: 'Textarea' },
    },
  )
