import { storiesOf } from '@storybook/vue'
import KImageFile from '../../components/KImageFile'
import KField from '../../components/KField'
import { withKnobs } from '@storybook/addon-knobs'

storiesOf('Components/Form field', module)
  .addDecorator(withKnobs)
  .add(
    'Image file',
    () => ({
      components: { KImageFile, KField },
      data() {
        return {
          model: null,
          model2: null,
          model3: null,
        }
      },
      template: `<div>
  <k-field>
    <label class="k-field-label">Image holder</label>
    <k-image-file
      v-model="model"
    ></k-image-file>
  </k-field>
  <k-field>
    <label class="k-field-label">Required</label>
    <k-image-file
      required
      v-model="model2"
    ></k-image-file>
  </k-field>
  <k-field>
    <label class="k-field-label">With initial placeholders, value is empty</label>
    <k-image-file
      v-model="model3"
      :initialImageName="'Cat.jpg'"
      :initialImageSource="'http://placekitten.com/200/300'"
      required
    />
  </k-field>

  <k-field>
    <label class="k-field-label">Disabled</label>
    <k-image-file
      disabled
    ></k-image-file>
  </k-field>

</div>
        `,
    }),
    { info: { summary: 'Image uploading' } },
  )
