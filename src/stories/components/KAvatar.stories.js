import { storiesOf } from '@storybook/vue'
import KAvatar from '../../components/KAvatar'
import { withKnobs, boolean } from '@storybook/addon-knobs'

storiesOf('Components/Avatar', module)
  .addDecorator(withKnobs)
  .add(
    'Avatars',
    () => ({
      components: { KAvatar },
      data() {
        return {
          rounded: boolean('Rounded', false),
        }
      },
      template: `
        <div>
          <ul>
            <li>
              <k-avatar src="broken.jpg" name="Hello Dolly Junior"/>
              Broken image automatically fallback to initials
            </li>
            <li>
              <k-avatar name="Some"/>
              Without image
            </li>
            <li>
              <k-avatar name="Y" blue/>
              Blue
            </li>
            <li>
              <k-avatar name="M" purple/>
              Purple
            </li>
            <li>
              <k-avatar name="T" blue semi-rounded/>
              Semi rounded
            </li>
            <li>
              <k-avatar name="G" blue semi-rounded medium/>
              Semi rounded medium
            </li>
            <li>
              <k-avatar src="https://placekitten.com/160/160" name="T" purple semi-rounded big/>
              Semi rounded Big
            </li>
            <li>
              <k-avatar src="https://placekitten.com/80/80" name="Some"/>
              <k-avatar src="https://placekitten.com/80/80" name="Some" :rounded="true"/>
              Normal image
            </li>
            <li>
              <k-avatar src="https://placekitten.com/360/640" name="Some" cover rounded/>
              <k-avatar src="https://placekitten.com/640/360" name="Some" cover/>
              Use cover for oversized image
            </li>
            <li>
              <k-avatar src="https://placekitten.com/10/10" name="Some" cover/>
              Or small image
            </li>
          </ul>

        </div>
      `,
    }),
    {
      info: {
        summary: 'Avatars',
      },
    },
  )
