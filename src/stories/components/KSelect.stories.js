import { storiesOf } from '@storybook/vue'
import KSelect from '../../components/KSelect'

storiesOf('Components/Select', module)
  .add(
    'Select',
    () => ({
      components: { KSelect },
      data() {
        return {
          val: 'John',
          value: null,
          autocomplete: null,
          options: ['John', 'Doe', 'Sunny', 'Smiles'],
          tags: [],
          custom: null,
          arrayOfIds: [],
          customOptions: [
            { title: 'Space Pirate', desc: 'More space battles!', img: 'https://loremflickr.com/630/360', id: 1 },
            { title: 'Merchant', desc: 'PROFIT!', img: 'https://loremflickr.com/620/360', id: 2 },
            { title: 'Explorer', desc: 'Discovering new species!', img: 'https://loremflickr.com/610/360', id: 3 },
            { title: 'Miner', desc: 'We need to go deeper!', img: 'https://loremflickr.com/640/350', id: 4 },
          ],
          objOptions: [{ label: 'Sunny', code: 'sunny' }, { label: 'Smiles', code: 'smiles' }],
          keys: [],
          key: null,
        }
      },
      computed: {
        bindedOptions() {
          return this.customOptions.map(option => ({ label: option.title, id: option.id }))
        },
      },
      methods: {
        assignVal(v) {
          this.val = v
        },
      },
      template: `
        <div>
          <h5>Our select is a simple styled wrapper for <a href="https://vue-select.org/" target="_blank">
            vue-select</a> just use
            all features of it</h5>
          <k-select
            v-model="keys"
            :options="objOptions"
            :reduce="option => option.code"
            name="options"
            selectName="Object options taggable multiple"
            multiple
            taggable
            purple
            purple-label
          >
            <pre>{{keys}}</pre>
          </k-select>
          <k-select placeholder="Loading" loading purple/>
          <k-select
            selectName="Object options"
            :options="objOptions"
            v-model="key"
            name="options"
            :reduce="option => option.code"
            purple
          >
          <pre>{{key}}</pre>
          </k-select>
          <k-select
            name="ids"
            v-model="arrayOfIds"
            selectName="Binded options"
            :reduce="option => option.id"
            :options="bindedOptions"
            placeholder="Binded objects"
            multiple
            taggable
          >
            <pre>{{arrayOfIds}}</pre>
          </k-select>

          <k-select
            :value='val'
            required
            @input="assignVal"
            :options="options"
            placeholder="Select one"
            selectName="Select Label"
          />

          <pre>  {{val}}</pre>
          <hr>
          <k-select v-model="value" :options="options" placeholder="Select option" selectName="Select Label">
            <div slot="helper">
              <span class="k-select-helper">Helper text</span>
            </div>
          </k-select>
          <k-select
            v-model="value"
            :options="options"
            placeholder="Selected option is invalid"
            selectName="Invalid option"
            invalid
          >
            <div slot="helper">
              <span class="k-select-error">Error text</span>
            </div>
          </k-select>
          <k-select
            v-model="autocomplete"
            :options="options"
            placeholder="Pick some"
            selectName="Autocomplete"
            searchable
            :preserve-search="true"
          ></k-select>
          <pre>{{autocomplete}}</pre>
          <hr>
          <form action="" id="form">

            <k-select
              v-model="tags"
              :options="options"
              placeholder="Select option"
              :multiple="true"
              :taggable="true"
              selectName="Taggable"
              name="multiple"
            ></k-select>
          </form>
          <pre>{{tags}}</pre>
          <hr>
          <k-select
            v-model="value"
            :options="options"
            placeholder="Select option"
            selectName="Disabled select"
            disabled
          ></k-select>
          <k-select
            v-model="custom"
            :options="customOptions"
            :valuePicker="option => option.title"
            placeholder="Custom option"
            selectName="Custom options"
          >
            <template slot="singleLabel" slot-scope="props" style="display: flex">
              <img class="option__image" :src="props.option.img" alt="No Man’s Sky"
                   style="max-width: 80px; max-height: 40px">
              <span class="option__desc"><span class="option__title">{{ props.option.title }}</span></span>
            </template>
            <template slot="option" slot-scope="props">
              <div style="display: flex;">
                <img
                  class="option__image"
                  :src="props.img"
                  :alt="props.title"
                  style="display: inline-flex; max-width: 100px"
                >
                <div class="option__desc"
                     style="display: flex; flex-direction: column; justify-content: space-evenly; margin-left: 10px;">
                  <div class="option__title" style="font-weight: 900">{{ props.title }}</div>
                  <div class="option__small">{{ props.desc }}</div>
                </div>
              </div>
            </template>
            {{custom}}
          </k-select>
        </div>`,
    }),
    { info: { summary: 'Select' } },
  )
