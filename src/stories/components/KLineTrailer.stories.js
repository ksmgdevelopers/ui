import { storiesOf } from '@storybook/vue'
import KLineTrailer from '../../components/KLineTrailer'

storiesOf('Components/Line trailer', module)
  .add(
    'Trail Line',
    () => ({
      components: {
        KLineTrailer,
      },
      template: `<div>
  <k-line-trailer>Hello</k-line-trailer>
  <k-line-trailer left right bold-line><h1>World</h1></k-line-trailer>
  <k-line-trailer blue slim><h2>Trailer slim</h2></k-line-trailer>
  <k-line-trailer gray><h3>Header</h3></k-line-trailer>
  <k-line-trailer gray-line><h4>Header</h4></k-line-trailer>
  <k-line-trailer blue black-line><h5>Header</h5></k-line-trailer>
  <k-line-trailer left :right="false" blue-line><h2>Start</h2></k-line-trailer>
  <k-line-trailer><img src="https://i.pravatar.cc/40" alt=""></k-line-trailer>
</div>`,
    }),
    {
      info: {
        summary: 'Line trailer',
      },
    },
  )
