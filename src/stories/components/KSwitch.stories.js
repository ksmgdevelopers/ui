import { storiesOf } from '@storybook/vue'
import KSwitch from '../../components/KSwitch'
import { withKnobs, text, boolean } from '@storybook/addon-knobs'

storiesOf('Components/Form field', module)
  .addDecorator(withKnobs)
  .add(
    'Switch',
    () => ({
      components: { KSwitch },
      data: () => ({
        firstCheck: false,
        noMargin: false,
        noLabel: true,
        stringCheck: null,
        arrayCheck: [],
        label: text('Label', 'Switch me. I am string'),
        disabled: boolean('Disabled', true),
      }),
      template: `
        <div>
          <k-switch v-model="firstCheck" purple>Switch me. I am boolean and purple</k-switch>
          <pre>{{firstCheck}}</pre>
          <k-switch v-model="stringCheck" value="Accepted" required>{{label}}</k-switch>
          <pre>{{stringCheck}}</pre>
          <k-switch v-model="arrayCheck" value="one">I am part of array</k-switch>
          <k-switch v-model="arrayCheck" value="two">Me too</k-switch>
          <pre>{{arrayCheck}}</pre>
          <k-switch v-model="firstCheck" :disabled="disabled" :required="true">I can be <a href="#">disabled</a>
          </k-switch>
          <hr>
          <k-switch v-model="noMargin" noMargin>No margin</k-switch>
          <k-switch v-model="noMargin" disabled noMargin>No margin</k-switch>
          <hr>
          No label <br>

          <k-switch v-model="noLabel" noMargin/>
          <k-switch v-model="noLabel" noMargin disabled/>
        </div>
      `,
    }),
    {
      info: {
        summary: `Switch button`,
      },
    },
  )
