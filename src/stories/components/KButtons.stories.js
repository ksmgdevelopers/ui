import KRoundedButton from '../../components/KRoundedButton'
import { storiesOf } from '@storybook/vue'
import { withKnobs, text, boolean } from '@storybook/addon-knobs'
import { action } from '@storybook/addon-actions'
import KTabButton from '../../components/KTabButton'
import KButton from '../../components/KButton'
import KTabItem from '../../components/KTabItem'
import KIcon from '../../components/KIcon'

storiesOf('Components/Buttons', module)
  .addDecorator(withKnobs)
  .add(
    'Rounded button',
    () => ({
      components: { KRoundedButton },
      props: {
        disabled: { default: boolean('Disabled', false) },
        secondary: { default: boolean('Secondary', false) },
        cancel: { default: text('Cancel text', 'Cancel') },
      },
      methods: {
        text,
        clickHandler: action('Clicked'),
        clickHandler2: action('Clicked 2'),
      },
      data() {
        return {
          loading: boolean('Loading', false),
        }
      },
      template: `
        <div>
          <h5 style="margin-bottom: 0">Regular</h5>
          <k-rounded-button
            :secondary="secondary"
            :disabled="disabled"
            @click="clickHandler"
            :loading="loading"
          >{{text('Slot', 'Submit')}}
          </k-rounded-button>
          <k-rounded-button disabled purple>Disabled</k-rounded-button>
          <h5 style="margin-bottom: 0">Small with gradient</h5>
          <k-rounded-button
            :secondary="secondary"
            :disabled="disabled"
            @click="clickHandler2"
            size="small"
            :loading="loading"
            gradient
          >
            {{ text('Slot', 'Submit')}}
          </k-rounded-button>
          <k-rounded-button
            purple
            :disabled="disabled"
            @click="clickHandler2"
            size="small"
            :loading="loading"
          >
            {{ text('Slot', 'Submit')}}
          </k-rounded-button>
          <k-rounded-button
            purple
            gradient
            loading
            :disabled="disabled"
            @click="clickHandler2"
            size="small"
          >
            {{ text('Slot', 'Submit')}}
          </k-rounded-button>
          <k-rounded-button
            :secondary="secondary"
            :disabled="disabled"
            @click="clickHandler2"
            size="small"
            loading
            gradient
          >
            {{ text('Slot', 'Submit')}}
          </k-rounded-button>

          <hr>
          <h5>Grouped</h5>
          <k-rounded-button secondary grouped>{{cancel}}</k-rounded-button>
          <k-rounded-button>{{text('Slot', 'Submit')}}</k-rounded-button>
          <hr>
          <k-rounded-button secondary grouped size="small">{{cancel}}</k-rounded-button>
          <k-rounded-button size="small">{{text('Slot', 'Submit')}}</k-rounded-button>
          <k-rounded-button href="/" size="small">Link</k-rounded-button>
          <hr>
        </div>
      `,
    }),
    {
      info: {
        summary: 'Summary',
      },
    },
  )
  .add(
    'Grouped buttons',
    () => ({
      components: { KTabButton },
      template: `
        <div>
          <h5>Medium</h5>
          <div style="display: flex;">
            <div class="k-tabs">
              <k-tab-button>Home</k-tab-button>
              <k-tab-button active>Active</k-tab-button>
              <k-tab-button>Demo</k-tab-button>
              <k-tab-button href="#">Link</k-tab-button>
              <k-tab-button disabled>Disabled</k-tab-button>
            </div>
            <k-tab-button style="margin-left: 10px">Stand alone button</k-tab-button>
          </div>
          <h5>Small</h5>
          <div style="display: flex;">
            <div class="k-tabs">
              <k-tab-button size="small">Home</k-tab-button>
              <k-tab-button size="small" active>Active</k-tab-button>
              <k-tab-button size="small">Demo</k-tab-button>
              <k-tab-button size="small" href="#">Link</k-tab-button>
              <k-tab-button size="small" disabled>Disabled</k-tab-button>
            </div>
            <k-tab-button size="small" style="margin-left: 10px">Stand alone button</k-tab-button>
          </div>
          <hr>
          <h5 style="margin-bottom: 0;">Minimal</h5>
          <div style="display: flex; background-color: #333; padding: 5px">
            <nav class="k-tabs k-tabs--horizontal">
              <k-tab-button size="small" minimal beige>Home</k-tab-button>
              <k-tab-button size="small" minimal beige active>Mountains</k-tab-button>
            </nav>
          </div>
          <div style="display: flex; background-color: white; padding: 5px">
            <nav class="k-tabs k-tabs--horizontal">
              <k-tab-button size="small" minimal purple>Home</k-tab-button>
              <k-tab-button size="small" minimal purple active>Mountains</k-tab-button>
            </nav>
          </div>
          <div style="display: flex; background-color: white; padding: 5px">
            <nav class="k-tabs k-tabs--horizontal">
              <k-tab-button size="small" minimal blue>Home</k-tab-button>
              <k-tab-button size="small" minimal blue active>Mountains</k-tab-button>
            </nav>
          </div>
        </div>
      `,
    }),
    { info: { summary: 'Tabs' } },
  )
  .add(
    'Default buttons',
    () => ({
      components: { KButton, KIcon },
      methods: {
        onClick: action('Click handler'),
      },
      template: `
        <div>
          <k-button>Default button</k-button>
          <k-button disabled @click="onClick">Default button</k-button>
          <k-button primary>Primary button</k-button>
          <k-button primary @click="onClick">
            <k-icon name="close"></k-icon>
            Icon button
          </k-button>
          <k-button danger>Danger button</k-button>
        </div>
      `,
    }),
    { info: { summary: 'Default Buttons' } },
  )
  .add(
    'Tabs',
    () => ({
      components: { KTabItem },
      methods: {
        onClick(event) {
          this.active = event.target.href
        },
      },
      data() {
        return { active: '#home' }
      },
      template: `
        <div style="background: white; display: flex">
          <k-tab-item href="#home" @click.prevent="active = '#home'" :active="'#home' === active">Home</k-tab-item>
          <k-tab-item href="#demo" @click.prevent="active = '#demo'" :active="'#demo' === active">Demo</k-tab-item>
          <k-tab-item href="#contacts" @click.prevent="active = '#contacts'" :active="'#contacts' === active">Contacts
          </k-tab-item>
          <k-tab-item href="#disabled" @click.prevent :active="'#disabled' === active" disabled>Disabled</k-tab-item>
        </div>
      `,
    }),
    { info: { summary: 'Tabs' } },
  )
