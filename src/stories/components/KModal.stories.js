import { storiesOf } from '@storybook/vue'
import { withKnobs } from '@storybook/addon-knobs'
import KModal from '../../components/KModal'
import KButton from '../../components/KButton'
import KTabItem from '../../components/KTabItem'

storiesOf('Components/Modal', module)
  .addDecorator(withKnobs)
  .add(
    'Modal dialogs',
    () => ({
      components: { KModal, KButton, KTabItem },
      methods: {
        toggle(key) {
          this[key] = !this[key]
        },
      },
      data() {
        return {
          active: '#home',
          simple: false,
          spaced: false,
          noSpace: false,
          full: false,
          escape: false,
          button: false,
          outside: false,
        }
      },
      template: `
        <div>
          <k-button primary @click="toggle('simple')">Simple modal</k-button>
          <k-modal :active.sync="simple" @close="toggle('simple')">
            Simple modal
          </k-modal>

          <k-button primary @click="toggle('spaced')">Spaced modal</k-button>
          <k-modal :active.sync="spaced" spaced @close="toggle('spaced')">
            <template slot="header">
              Talent software
            </template>
            Spaced modal
            <br>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. At fugiat in laborum nam nisi sint, vero.
            <div slot="footer" style="display: flex; justify-content:  flex-end">
              <k-button primary @click="toggle('spaced')" style="margin-right: 5px">Save</k-button>
              <k-button danger @click="toggle('spaced')">Close</k-button>
            </div>
          </k-modal>

          <k-button primary @click="toggle('noSpace')">No Spaced modal</k-button>
          <k-modal :active.sync="noSpace" no-space @close="toggle('noSpace')">
            <template slot="header">
              Talent software
            </template>
            No Spaced modal
            <br>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. At fugiat in laborum nam nisi sint, vero.
            <div slot="footer" style="display: flex; justify-content:  flex-end">
              <k-button primary @click="toggle('spaced')" style="margin-right: 5px">Save</k-button>
              <k-button danger @click="toggle('spaced')">Close</k-button>
            </div>
          </k-modal>

          <k-button primary @click="toggle('escape')">No close on esc modal</k-button>
          <k-modal :active.sync="escape" @close="toggle('escape')" :close-on-esc="false">
            No close on esc modal
          </k-modal>

          <k-button primary @click="toggle('button')">With close button</k-button>
          <k-modal :active.sync="button" @close="toggle('button')" :close-button="true">
            With close button
          </k-modal>

          <k-button primary @click="toggle('outside')">No close on outside click</k-button>
          <k-modal :active.sync="outside" @close="toggle('outside')" :clickOutsideToClose="false" close-button
                   header="Click">
            No close on outside click
          </k-modal>

          <k-button primary @click="toggle('full')">Full modal</k-button>
          <k-modal :active.sync="full" @close="toggle('full')">
            <template slot="top">
              <k-tab-item
                href="#home"
                style="flex-grow: 1; text-align: center"
                @click.prevent="active = '#home'"
                :active="'#home' === active"
              >
                Tab
              </k-tab-item>
              <k-tab-item
                href="#demo"
                style="flex-grow: 1; text-align: center"
                @click.prevent="active = '#demo'"
                :active="'#demo' === active"
              >
                Demo
              </k-tab-item>
            </template>
            <template slot="header">
              Talent software
            </template>
            Talent management systems, applications and bots developed to make it simpler for employers and talent to
            find and
            match each other
            <div slot="footer" style="display: flex; justify-content:  flex-end">
              <k-button primary @click="toggle('full')" style="margin-right: 5px">Save</k-button>
              <k-button danger @click="toggle('full')">Close</k-button>
            </div>
            <template slot="bottom">
              <k-button style="width: 100%;">All</k-button>
            </template>
          </k-modal>

        </div>
      `,
    }),
    {
      info: { summary: 'Modals' },
    },
  )
