import { storiesOf } from '@storybook/vue'
import KRating from '../../components/KRating'
import { number, withKnobs } from '@storybook/addon-knobs'

storiesOf('Components/Rating', module)
  .addDecorator(withKnobs)
  .add(
    'Rating',
    () => ({
      components: { KRating },
      data() {
        return {
          model: 2,
          hovered: null,
          intervaled: 0,
          rating: number('Rating', 2),
        }
      },
      methods: {
        changeHovered(val) {
          this.hovered = val
        },
      },
      mounted() {
        setInterval(() => {
          this.intervaled = Math.floor(Math.random() * 10 % 6)
        }, 1000)
      },
      template: `
        <div>
          <h4 style="margin-bottom: 0">Size</h4>
          <k-rating :rating="rating" name="small" small/>
          <k-rating :rating="intervaled" name="medium"/> 
          <k-rating name="large" large/>
          <k-rating name="custom" :starSize="40"/>
          <h5 style="margin-bottom: 0">Simple</h5>
          <k-rating
            v-model="model"
            @current-rating="changeHovered"
            @reset-rating="changeHovered(null)"
            inline
            name="rating"
          />
          <pre style="display: inline-flex; background: #c9c9c9; padding: 2px 5px; margin-left: 10px;">Value: {{model}}
            <span
              v-if="hovered">. Hovered value: {{hovered}}</span></pre>

          <h5 style="margin-bottom: 0">Any colors</h5>
          <k-rating name="colors" v-model="model" active-color="red" inactive-color="green"/>

          <h5 style="margin-bottom: 0">Read only</h5>
          <k-rating name="rating" readonly :rating="3.5" :increment="0.5"/>
        </div>
      `,
    }),
    {
      info: { summary: 'Rating widget' },
    },
  )
