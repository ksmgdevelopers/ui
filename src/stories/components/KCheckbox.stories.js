import { storiesOf } from '@storybook/vue'
import KCheckbox from '../../components/KCheckbox'
import { withKnobs, text, boolean } from '@storybook/addon-knobs'

storiesOf('Components/Checkbox', module)
  .addDecorator(withKnobs)
  .add(
    'Checkbox with label',
    () => ({
      components: { KCheckbox },
      data: () => ({
        array: [],
        firstcheck: false,
        label: text('Label', 'Agree'),
        checked: boolean('Checked', true),
        disabled: boolean('Disabled', true),
        check: false,
      }),
      template: `
        <div>
        <k-checkbox
          name="agree"
          v-model="array"
          :label="label"
        />
        <k-checkbox v-model="array" :force-checked="false">Agree with <a href="#">terms</a></k-checkbox>
        <k-checkbox v-model="array" :disabled="disabled" required>
          Agree with <a href="#">terms</a>
        </k-checkbox>
        <k-checkbox :checked="check" @change="check = !check">
          Checked {{ check }} with attr
          <template slot="bottom">
            <div class="k-select-error">Error</div>
          </template>
        </k-checkbox>
        </div>`,
    }), {
      info: {
        summary: 'Info',
      },
    },
  )
  .add(
    'Checkbox Group',
    () => ({
      components: { KCheckbox },
      data() {
        return {
          model: true,
          obj: {},
          obja: { a: 'b' },
          objb: { c: 'd' },
          array: [],
        }
      },
      template: `
        <div>
        <div class="k-label k-label--required">Checkbox group</div>
        <k-checkbox id="cb1" v-model="model" value="string" label="String no-margin" no-margin/>
        <br>
        <k-checkbox id="cb2" v-model="model" :value="true" purple>Boolean and purple</k-checkbox>
        <br>
        <k-checkbox id="cb4" v-model="model" :value="false" disabled>Boolean</k-checkbox>
        <br>
        <div>Model: ({{ typeof model }}) {{ model }}</div>
        <hr>
        <div class="k-label">Objects needs to be observable</div>
        <k-checkbox id="cb3" v-model="obj" :value="obja">Object b</k-checkbox>
        <k-checkbox id="cb3" v-model="obj" :value="objb">Object c</k-checkbox>
        <div>Model: ({{ typeof obj }}) {{ obj }}</div>
        <hr>
        <div class="k-label">Checkbox group array</div>
        <k-checkbox id="cb5" v-model="array" :value="1" required>Array 1</k-checkbox>
        <k-checkbox id="cb6" v-model="array" :value="'string'">Array 2</k-checkbox>
        <div>Model: {{ array }}</div>
        </div>
      `,
    }),
    {
      info: {
        summary: 'Info',
      },
    },
  )
