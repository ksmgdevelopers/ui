import { storiesOf } from '@storybook/vue'
import IconHolder from '../helpers/IconHolder'
import KIcon from '../../components/KIcon'

const req = require.context('../../assets/icons', true, /.*\.svg/im)
const icons = req.keys().map(key => key.slice(2, -4))

storiesOf('Components/Icons', module)
  .add(
    'Inline icons',
    () => ({
      components: { KIcon, IconHolder },
      data() {
        return {
          icons,
        }
      },
      template: `<div>
  <h4>Use this component for single element icons</h4>
  <p>For big lists of icons try to use cacheable icons like icon sprites</p>
  <h5 style="margin-bottom: 5px">Icon sizing</h5>
  <div style="display: flex; flex-wrap: wrap; ">

    <icon-holder name="small">
      <k-icon name="upload" small></k-icon>
    </icon-holder>
    <icon-holder name="medium">
      <k-icon name="upload"></k-icon>
    </icon-holder>
    <icon-holder name="large">
      <k-icon name="upload" large></k-icon>
    </icon-holder>
    <icon-holder name="xlarge">
      <k-icon name="upload" xlarge></k-icon>
    </icon-holder>

  </div>
  <h5 style=" margin-top: 20px; margin-bottom: 5px;">Library</h5>
  <div style="display: flex; flex-wrap: wrap;">

    <icon-holder :name="icon" :key="icon" v-for="icon in icons">
      <k-icon :name="icon"></k-icon>
    </icon-holder>

  </div>
</div>
      `,
    }),
    { info: { summary: 'Sprite icon' } },
  )
