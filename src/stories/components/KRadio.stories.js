import KRadio from '../../components/KRadio'
import { storiesOf } from '@storybook/vue'
import { withKnobs, boolean, text } from '@storybook/addon-knobs'

storiesOf('Components/Radio', module)
  .addDecorator(withKnobs)
  .add(
    'Simple radio',
    () => ({
      components: { KRadio },
      data: () => ({
        initial: false,
        label: text('Label', 'Variant A'),
        c: false,
      }),
      template: `
        <div>
          <k-radio
            v-model="initial"
            :value="true"
            :label="label"
          />
          <k-radio
            v-model="initial"
            value="okay"
            label="Okay?"
            purple
          />
          <k-radio
            v-model="initial"
            :checked="false"
            disabled
            label="Variant B"
          />
          <br>
          <k-radio
            v-model="initial"
            :value="false"
            label="False no margin"
          />
          <k-radio
            v-model="initial"
            :value="false"
            required
            label="False no margin required"
          />
          <pre>
            model: {{initial}}
          </pre>
        </div>`,
    }),
    {
      info: {
        summary: ` Radio button `,
      },
    },
  )
  .add(
    'Radio group',
    () => ({
      data() {
        return {
          radio: true,
          obj: { a: 'b' },
        }
      },
      components: { KRadio },
      template: `
        <div>
          <span class="k-label">Radio group</span>
          <div>
            <k-radio v-model="radio" value="Oh, yes">String</k-radio>
            <k-radio v-model="radio" :value="false">Boolean</k-radio>
            <k-radio v-model="radio" :value="obj">Object</k-radio>
            <k-radio v-model="radio">Nothing</k-radio>
            <k-radio v-model="radio" :value="true" disabled>Disabled</k-radio>

            <div>Model: ({{typeof radio}}) {{radio}}</div>
          </div>
        </div>
      `,
    }),
    { info: { summary: '' } },
  )
