import { storiesOf } from '@storybook/vue'
import KFile from '../../components/KFile'
import KField from '../../components/KField'

storiesOf('Components/Form field', module)
  .add(
    'File input',
    () => ({
      components: { KFile, KField },
      template: `
        <div>
          <h5>Model return only file names. Use k-change event to receive file list on each file change</h5>
          <k-field>
            <label class="k-field-label">Single</label>
            <k-file name="file"></k-file>
            <div slot="helper" class="k-field-helper">Helper text</div>
          </k-field>
          <k-field>
            <label class="k-field-label">Required</label>
            <k-file name="required" required/>
            <div slot="helper" class="k-field-helper">Helper text</div>
          </k-field>
          <k-field>
            <label class="k-field-label" for="image">Only images</label>
            <k-file id="image" name="image" accept="image/*"></k-file>
            <div slot="helper" class="k-field-helper">Helper text</div>
          </k-field>

          <k-field invalid>
            <label class="k-field-label" for="multiple">Multiple</label>
            <k-file id="multiple" multiple name="files[]">Multiple</k-file>
            <div slot="helper">
              <span class="k-field-error">Error text 1</span>
              <span class="k-field-error">Error text 2</span>
              <span class="k-field-error">Error text 3</span>
            </div>
          </k-field>

          <k-field>
            <label class="k-field-label" for="disabled">Disabled</label>
            <k-file id="disabled" disabled name="files[]">Disabled</k-file>
          </k-field>
        </div>`,
    }),
    { info: { summary: 'File input' } },
  )
