import { storiesOf } from '@storybook/vue'
import KSkeleton from '../../components/KSkeleton'

storiesOf('Components/Skeleton', module)
  .add(
    'Skeleton',
    () => ({
      components: { KSkeleton },
      template: `
        <div>
          <k-skeleton></k-skeleton>
          <h5 style="margin-bottom: 0;">Boilerplate</h5>
          <k-skeleton boilerplate/>
          <h5 style="margin-bottom: 0;">Avatar</h5>
          <k-skeleton avatar/>
          <h5 style="margin-bottom: 0;">Sizing</h5>
          <k-skeleton width="40%" height="5vh" max-width="100%" :max-height="40"/>
        </div>
      `,
    }),
    { info: { summary: 'Skeleton' } },
  )
