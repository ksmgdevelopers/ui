import { storiesOf } from '@storybook/vue'
import KDropMenu from '../../components/KDropMenu'
import KDropMenuContent from '../../components/KDropMenuContent'
import KDropMenuItem from '../../components/KDropMenuItem'
import KButton from '../../components/KButton'

storiesOf('Components/DropMenu', module)
  .add(
    'Simple',
    () => ({
      components: { KDropMenu, KDropMenuContent, KDropMenuItem, KButton },
      template: `
        <div>
        <k-drop-menu :offset="[0, 5]" placement="bottom-start" :close-on-select="false">
          <k-button menu-trigger>Bottom start no close on select</k-button>
          <k-drop-menu-content title="Menu">
            <k-drop-menu-item href="#">Link</k-drop-menu-item>
            <k-drop-menu-item type="button">Button</k-drop-menu-item>
            <k-drop-menu-item>
              <hr style="margin:0"/>
            </k-drop-menu-item>
            <k-drop-menu-item active>Router link supported as well</k-drop-menu-item>
          </k-drop-menu-content>
        </k-drop-menu>
        <k-drop-menu :offset="[0, 5]" placement="bottom-end" :close-on-click-outside="false">
          <k-button menu-trigger>Bottom end no close on outside click</k-button>
          <k-drop-menu-content title="Menu">
            <k-drop-menu-item href="#">Link</k-drop-menu-item>
            <k-drop-menu-item type="button">Button</k-drop-menu-item>
            <k-drop-menu-item active>Router link supported as well</k-drop-menu-item>
          </k-drop-menu-content>
        </k-drop-menu>
        <hr style="margin-top: 50px;">
        <k-drop-menu placement="top-start" :offset="[0, 5]" :close-on-click-outside="false" :close-on-select="false">
          <k-button menu-trigger>Place it top start no close on select or click outside</k-button>
          <k-drop-menu-content title="Menu">
            <k-drop-menu-item href="#">Link</k-drop-menu-item>
            <k-drop-menu-item type="button">Button</k-drop-menu-item>
            <k-drop-menu-item active>Router link supported as well</k-drop-menu-item>
          </k-drop-menu-content>
        </k-drop-menu>
        <k-drop-menu placement="top-end" :offset="[0, 5]">
          <k-button menu-trigger>Place it top end</k-button>
          <k-drop-menu-content title="Menu">
            <k-drop-menu-item href="#">Link</k-drop-menu-item>
            <k-drop-menu-item type="button">Button</k-drop-menu-item>
            <k-drop-menu-item active>Router link supported as well</k-drop-menu-item>
          </k-drop-menu-content>
        </k-drop-menu>
        <hr>
        <k-drop-menu placement="bottom" :offset="[0, 5]">
          <k-button menu-trigger>Bottom Bottom Bottom Bottom</k-button>
          <k-drop-menu-content title="Menu">
            <k-drop-menu-item href="#">Link</k-drop-menu-item>
            <k-drop-menu-item type="button">Button</k-drop-menu-item>
            <k-drop-menu-item active>Router link supported as well</k-drop-menu-item>
          </k-drop-menu-content>
        </k-drop-menu>
        <k-drop-menu placement="top" :offset="[0, 5]">
          <k-button menu-trigger>Top Top Top Top Top</k-button>
          <k-drop-menu-content title="Menu">
            <k-drop-menu-item href="#">Link</k-drop-menu-item>
            <k-drop-menu-item type="button">Button</k-drop-menu-item>
            <k-drop-menu-item active>Router link supported as well</k-drop-menu-item>
          </k-drop-menu-content>
        </k-drop-menu>
        <k-drop-menu placement="top" :offset="[35, -25]">
          <k-button menu-trigger>Top With custom offset</k-button>
          <k-drop-menu-content title="Menu">
            <k-drop-menu-item href="#">Link</k-drop-menu-item>
            <k-drop-menu-item type="button">Button</k-drop-menu-item>
            <k-drop-menu-item active>Router link supported as well</k-drop-menu-item>
          </k-drop-menu-content>
        </k-drop-menu>
        </div>
      `,
    }),
    { info: { summary: 'Drop Menu' } },
  )
