import { storiesOf } from '@storybook/vue'
import { withKnobs } from '@storybook/addon-knobs'
import KCard from '../../components/KCard'
import KTabItem from '../../components/KTabItem'
import KButton from '../../components/KButton'

storiesOf('Components/Card', module)
  .addDecorator(withKnobs)
  .add(
    'Simple card',
    () => ({
      components: { KCard, KTabItem, KButton },
      data() {
        return {
          active: '#home',
        }
      },
      template: `
        <div style="display: flex; flex-wrap: wrap; flex-direction: column; background-color: #fdf4ec;">
          <div style="display: flex; width: 100%">
            <k-card style="max-width: 320px; margin-right: 10px; ">
              <template slot="top">
                <div style="display: flex">
                  <k-tab-item href="#home" @click.prevent="active = '#home'" :active="'#home' === active">Home
                  </k-tab-item>
                  <k-tab-item href="#demo" @click.prevent="active = '#demo'" :active="'#demo' === active">Demo
                  </k-tab-item>
                  <k-tab-item href="#contacts" @click.prevent="active = '#contacts'" :active="'#contacts' === active">
                    Contacts
                  </k-tab-item>
                  <k-tab-item href="#disabled" @click.prevent :active="'#disabled' === active" disabled>Disabled
                  </k-tab-item>
                </div>
              </template>
              <div slot="header">Without shadow on hover</div>
              <div>We spread messages that produce results
                Every year we help over 500 companies spread messages and deliver results through social media. Almost
                10
                years
                ago, we were first in the market in social media recruitment and have since built up a solid knowledge
                and
                experience in the field.
              </div>
              <div slot="footer">Footer with smaller padding</div>
              <template slot="bottom">
                <k-button style="width: 100%; border-bottom: 0;border-right: 0;border-left: 0; border-radius: 0">Hello
                </k-button>
              </template>
            </k-card>

            <k-card style="max-width: 320px; margin-right: 10px;" outlined>
              <div slot="header">Outlined without shadow on hover</div>
              <div>We spread messages that produce results
                Every year we help over 500 companies spread messages and deliver results through social media. Almost
                10
                years
                ago, we were first in the market in social media recruitment and have since built up a solid knowledge
                and
                experience in the field.
              </div>
              <div slot="footer">Footer with smaller padding</div>
            </k-card>

            <k-card k-with-hover style="max-width: 320px">
              <div slot="header">With shadow on hover</div>
              <div>We spread messages that produce results
                Every year we help over 500 companies spread messages and deliver results through social media. Almost
                10
                years
                ago, we were first in the market in social media recruitment and have since built up a solid knowledge
                and
                experience in the field.
              </div>
              <div slot="footer">Footer with smaller padding</div>
            </k-card>
          </div>
          <hr/>
          <h5 style="margin-bottom:0">Color</h5>
          <div style="display: flex">
            <k-card bridesmaid style="max-width: 320px">
              <div slot="header">Bridesmaid</div>
              <div>We spread messages that produce results
                Every year we help over 500 companies spread messages and deliver results through social media. Almost
                10
                years
                ago, we were first in the market in social media recruitment and have since built up a solid knowledge
                and
                experience in the field.
              </div>
              <div slot="footer">Footer with smaller padding</div>
            </k-card>

          </div>
        </div>`,
    }),
    {
      info: {
        summary: 'Card',
      },
    },
  )
