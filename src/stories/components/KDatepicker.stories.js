import { storiesOf } from '@storybook/vue'
import KDatepicker from '../../components/KDatepicker'

storiesOf('Components/Datepicker', module)
  .add(
    'Datepicker',
    () => ({
      components: { KDatepicker },
      data() {
        return {
          dateModel: new Date(),
          today: new Date(),
        }
      },
      template: `
        <div>
          <h3>Datepicker is styled wrapper of <a
            href="https://github.com/charliekassel/vuejs-datepicker#readme"
            target="_blank"
          >vuejs-datepicker</a>
            feel free to use all of it's features</h3>
          <k-datepicker
            v-model="dateModel"
            :highlighted="{
              daysOfMonth: [1, 15, 31],
            }"
            format="yyyy-MM-dd"
            name="uniquename"
            label="Datepicker"
            placeholder="Choose a date"
            required
            purple
          >
            <div slot="helper">
              <span class="k-field-helper" slot="helper">Helper text</span>
            </div>
          </k-datepicker>
          <pre style="background: #c9c9c9; padding: 4px 8px">Model: {{dateModel}}</pre>
          <hr>
          <k-datepicker
            v-model="dateModel"
            name="Swedish"
            label="Swedish 🇸🇪"
            placeholder="Choose a date"
            swedish
          ></k-datepicker>
          <k-datepicker
            v-model="dateModel"
            name="disabled"
            label="Disabled without button, no margin"
            placeholder="Choose a date"
            disabled
            without-button
            no-margin
          >
          </k-datepicker>
          <k-datepicker
            :disabled-dates="{ from: new Date(Date.now() + 360000000 * 2), to: new Date(Date.now() - 360000000 * 2) }"
            name="inline"
            label="Inline calendar, highlight today"
            highlight-today
            inline
          />
          <k-datepicker
            invalid
            name="invalid"
            label="Datepicker"
            placeholder="Choose a date"
          >
            <div slot="helper">
              <div class="k-datepicker-error">Error</div>
              <div class="k-datepicker-error">Error too</div>
            </div>
          </k-datepicker>
        </div>`,
    }),
    { info: { summary: 'Datepicker' } },
  )
