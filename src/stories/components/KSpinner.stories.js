import { storiesOf } from '@storybook/vue'
import KSpinner from '../../components/KSpinner'
import { withKnobs } from '@storybook/addon-knobs'

storiesOf('Components/Spinner', module)
  .addDecorator(withKnobs)
  .add(
    'Simple',
    () => ({
      components: { KSpinner },
      template: `
        <div>
          <k-spinner/>
          <k-spinner color="red" :diameter="75" :strokeWidth="25"/>
          <k-spinner color="orange" :diameter="60" :strokeWidth="2"/>
          <k-spinner :diameter="40" :strokeWidth="5"/>
        </div>
      `,
    }),
    { info: { summary: 'Spinners' } },
  )
