import { storiesOf } from '@storybook/vue'
import KPagination from '../../components/KPagination'

storiesOf('Components/Pagination', module)
  .add(
    'Pagination',
    () => ({
      data: () => ({
        page: 50,
        forced: 50,
      }),
      components: { KPagination },
      methods: {
        setForced(page) {
          setTimeout(() => {
            this.forced = page
          }, 1000)
        },
      },
      template: `
        <div>
          <h2>Pagination</h2>
          <k-pagination :page-count="100"/>
          <h5 style="margin-bottom: 0;">Page range</h5>
          <k-pagination :page-count="100" :page-range="5" purple/>
          <h5 style="margin-bottom: 0;">Margin pages</h5>
          <k-pagination :page-count="100" :margin-pages="4"/>
          <h5 style="margin-bottom: 0;">Initial page</h5>
          <k-pagination :page-count="100" :margin-pages="2" v-model="page"/>
          <pre v-text="page"/>
          <h5 style="margin-bottom: 0;">Click handler with forced page, change after delay</h5>
          <k-pagination :page-count="100" :margin-pages="2" :value="forced" :force-page="forced"
                        :click-handler="setForced"/>
          <pre v-text="forced"/>
        </div>
      `,
    }),
    { info: { summary: 'Pagination' } },
  )
