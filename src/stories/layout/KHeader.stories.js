import { storiesOf } from '@storybook/vue'
import { withKnobs } from '@storybook/addon-knobs'
import KHeader from '../../components/layout/header/KHeader'
import KAvatar from '../../components/KAvatar'
import KSubheader from '../../components/layout/header/KSubheader'
import KIcon from '../../components/KIcon'
import KDropMenuContent from '../../components/KDropMenuContent'
import KDropMenu from '../../components/KDropMenu'
import KDropMenuItem from '../../components/KDropMenuItem'

storiesOf('Layout/Header', module)
  .addDecorator(withKnobs)
  .add(
    'Fixed',
    () => ({
      components: { KHeader, KAvatar, KSubheader },
      template: `
        <div style="background: #c5c5c5; position: relative; margin-bottom: 20px;">
          <k-header class="k-theme" fixed>
            <div class="k-header-row">
              <div class="k-header-section-start">
                <h2 style="margin: 0">KSMG</h2>
                <nav class="k-header-nav" aria-label="Main Menu" role="navigation">
                  <h2 class="k-header-nav--name">Main Menu</h2>
                  <ul class="k-header-nav-list" role="menubar">
                    <li class="k-header-nav-list--item" role="menuitem">
                      <a class="k-header-nav--link k-header-nav-link--active" href="#">Home</a>
                    </li>
                    <li class="k-header-nav-list--item" role="menuitem"><a class="k-header-nav--link"
                                                                           href="#">Contacts</a>
                    </li>
                    <li class="k-header-nav-list--item" role="menuitem"><a class="k-header-nav--link"
                                                                           href="#">Demo</a></li>
                  </ul>
                </nav>
              </div>
              <div class="k-header-section-end">
                <a href="#" class="k-header-action__link">+ New item</a>
                <k-avatar name="Profile" src="https://i.pravatar.cc/40"/>
              </div>
            </div>
          </k-header>

          <h1 style="padding-top: 65px; margin: 0">fixed header </h1>
          <div style="padding: 15px">
            <p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam, beatae consequatur ea expedita explicabo fuga inventore iusto nam numquam provident quam ullam voluptas voluptates. Aspernatur commodi harum quaerat ullam veniam!</span><span>Accusamus consequuntur dolore modi necessitatibus neque quis vel. A at autem cumque distinctio dolor eaque esse ex illum impedit minus, molestias nemo quibusdam quos repudiandae suscipit tempora totam ullam veritatis?</span>
            </p>
            <p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium alias aliquid commodi corporis culpa cumque dignissimos dolor dolores eius eum id natus nisi praesentium, repellendus rerum sapiente suscipit tempore temporibus!</span><span>Aliquam animi illum iste laudantium molestiae, nisi perferendis voluptate voluptatum! Aspernatur doloremque error explicabo, hic iure nobis quas reprehenderit. Nihil tempora ut voluptates? At dignissimos eos possimus ratione repellat voluptatem.</span>
            </p>
            <p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium alias aliquid commodi corporis culpa cumque dignissimos dolor dolores eius eum id natus nisi praesentium, repellendus rerum sapiente suscipit tempore temporibus!</span><span>Aliquam animi illum iste laudantium molestiae, nisi perferendis voluptate voluptatum! Aspernatur doloremque error explicabo, hic iure nobis quas reprehenderit. Nihil tempora ut voluptates? At dignissimos eos possimus ratione repellat voluptatem.</span>
            </p>
            <p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium alias aliquid commodi corporis culpa cumque dignissimos dolor dolores eius eum id natus nisi praesentium, repellendus rerum sapiente suscipit tempore temporibus!</span><span>Aliquam animi illum iste laudantium molestiae, nisi perferendis voluptate voluptatum! Aspernatur doloremque error explicabo, hic iure nobis quas reprehenderit. Nihil tempora ut voluptates? At dignissimos eos possimus ratione repellat voluptatem.</span>
            </p>
            <p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium alias aliquid commodi corporis culpa cumque dignissimos dolor dolores eius eum id natus nisi praesentium, repellendus rerum sapiente suscipit tempore temporibus!</span><span>Aliquam animi illum iste laudantium molestiae, nisi perferendis voluptate voluptatum! Aspernatur doloremque error explicabo, hic iure nobis quas reprehenderit. Nihil tempora ut voluptates? At dignissimos eos possimus ratione repellat voluptatem.</span>
            </p>
            <p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium alias aliquid commodi corporis culpa cumque dignissimos dolor dolores eius eum id natus nisi praesentium, repellendus rerum sapiente suscipit tempore temporibus!</span><span>Aliquam animi illum iste laudantium molestiae, nisi perferendis voluptate voluptatum! Aspernatur doloremque error explicabo, hic iure nobis quas reprehenderit. Nihil tempora ut voluptates? At dignissimos eos possimus ratione repellat voluptatem.</span>
            </p>
            <p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat minus, nam odit perspiciatis quis reiciendis sit ullam unde? Commodi dolor dolorem enim et minima molestias odio porro repellendus velit voluptate.</span><span>Iusto laudantium nemo nisi perferendis repudiandae sed vero voluptas! A ad autem consequatur cum delectus deleniti, distinctio eaque eligendi esse excepturi exercitationem, fugit ipsum natus quos recusandae sequi tempora voluptates.</span>
            </p>
          </div>
        </div>
      `,
    }),
    { info: { summary: 'Simple header' } },
  )
  .add('Static',
    () => ({
      components: { KHeader, KAvatar, KSubheader },
      template: `
        <div>
          <k-header class="k-theme">
            <div class="k-header-row">
              <div class="k-header-section-start">
                <h2 style="margin: 0">KSMG</h2>
                <nav class="k-header-nav" aria-label="Main Menu" role="navigation">
                  <h2 class="k-header-nav--name">Main Menu</h2>
                  <ul class="k-header-nav-list" role="menubar">
                    <li class="k-header-nav-list--item" role="menuitem">
                      <a class="k-header-nav--link k-header-nav-link--active" href="#">Home</a>
                    </li>
                    <li class="k-header-nav-list--item" role="menuitem"><a class="k-header-nav--link"
                                                                           href="#">Contacts</a></li>
                    <li class="k-header-nav-list--item" role="menuitem"><a class="k-header-nav--link" href="#">Demo</a>
                    </li>
                  </ul>
                </nav>
              </div>
              <div class="k-header-section-end">
                <a href="#" class="k-header-action__link">+ New item</a>
                <k-avatar name="Profile" src="https://i.pravatar.cc/40"></k-avatar>
              </div>
            </div>
          </k-header>
          <k-subheader>
            <h1>Hi</h1>
            <h2>Howdy</h2>
          </k-subheader>
          <div style="height: 50px; padding: 15px ">
            <p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis explicabo necessitatibus non perspiciatis tenetur! Aliquam animi atque beatae culpa eligendi molestias natus nobis pariatur placeat, sit, suscipit veritatis? A, nulla.</span><span>Ab aliquam at, delectus fuga in, libero minima mollitia nostrum quas quis rem, tempora unde vitae! Dolore eius esse praesentium quam. Alias aliquam delectus fugit itaque, quaerat quo vel voluptate.</span>
            </p>
            <p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A ad adipisci asperiores atque blanditiis corporis deserunt dolores earum facere, ipsam iure iusto labore officia quaerat ratione repellendus sed, sit tempore.</span><span>Ab ad aspernatur, consectetur consequuntur cum debitis dicta dolorum, eum fugit illum in ipsum laboriosam laudantium minima natus perspiciatis placeat quae quia quidem quod ratione similique vel veniam vero voluptate!</span>
            </p>
            <p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A ad consequuntur distinctio facere ipsa nam nemo nesciunt nostrum nulla odio officiis placeat possimus quaerat quia quidem repudiandae sequi sunt, tempore?</span><span>Autem consectetur cupiditate, deleniti dolor ducimus earum hic iste iure labore laboriosam laborum maxime, molestiae necessitatibus nobis odit officiis placeat provident quo rem sequi sunt ullam unde ut veritatis voluptas.</span>
            </p>
            <p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet aperiam blanditiis consequatur deleniti eaque explicabo fugiat illo, illum ipsa iure labore, laudantium omnis praesentium provident quibusdam quidem repellat vel?</span><span>Alias aut culpa cum, dignissimos facilis incidunt ipsa ipsum maiores natus nesciunt nihil placeat, quas voluptates? Ex impedit mollitia natus vero voluptas! Accusantium animi dolorum hic incidunt nostrum officiis rem.</span>
            </p>
            <p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, adipisci atque beatae blanditiis earum, enim fugiat inventore nihil non, possimus repellendus reprehenderit suscipit voluptatem. Assumenda corporis itaque laboriosam sequi vitae?</span><span>Adipisci, aspernatur at consequuntur error facilis id illum in maiores minima nam quod repellat vel. Error odio sequi velit veritatis. Consequatur distinctio doloremque perspiciatis qui repudiandae. Dolorem est repudiandae tenetur?</span>
            </p>
          </div>
        </div>
      `,
    }),
    { info: { summary: 'Fixed header' } },
  )
  .add('Color',
    () => ({
      components: { KHeader, KAvatar, KIcon, KDropMenu, KDropMenuItem, KDropMenuContent, KSubheader },
      template: `
        <div class="k-theme">
          <k-header blue>
            <div class="k-header-row">
              <div class="k-header-section-start">
                <a style="font-size: 16px; line-height: 12px; margin: 0; color: #fff">Cruitment</a>
                <nav class="k-header-nav" aria-label="Main Menu" role="navigation">
                  <h2 class="k-header-nav--name">Main Menu</h2>
                  <ul class="k-header-nav-list" role="menubar">
                    <li class="k-header-nav-list--item" role="menuitem">
                      <a class="k-header-nav--link k-header-nav-link--active" href="#">Home</a>
                    </li>
                    <li class="k-header-nav-list--item" role="menuitem">
                      <a class="k-header-nav--link" href="#">Contacts</a>
                    </li>
                    <li class="k-header-nav-list--item" role="menuitem"><a class="k-header-nav--link"
                                                                           href="#">Demo</a>
                    </li>
                  </ul>
                </nav>
              </div>
              <div class="k-header-section-end">
                <a href="#" class="k-header-action__link">+ New item</a>
                <a href="#" class="k-header-action__link router-link-exact-active">+ New item</a>
                <k-drop-menu placement="bottom-start">
                  <div class="k-header-action--item" data-menu-trigger>
                    <div class="k-header-action-item-title">Name</div>
                    <k-avatar name="Profile" src="https://i.pravatar.cc/40" small/>
                    <k-icon name="down-chevron" class="k-header-action-item--down"/>
                  </div>
                  <k-drop-menu-content title="Name">
                    <k-drop-menu-item><span class="k-drop-menu-item-content k-drop-menu-item-content--clickable">Item one</span>
                    </k-drop-menu-item>
                    <k-drop-menu-item><span
                      class="k-drop-menu-item-content k-drop-menu-item-content--clickable k-drop-menu-item-content--active">Second Acitve</span>
                    </k-drop-menu-item>
                    <k-drop-menu-item><span
                      class="k-drop-menu-item-content k-drop-menu-item-content--clickable">333</span>
                    </k-drop-menu-item>
                  </k-drop-menu-content>
                </k-drop-menu>
              </div>
            </div>
          </k-header>

          <h3 style="margin: 20px 0 0;">Gradient subheader</h3>
          <k-header blue>
            blue
          </k-header>
          <k-subheader gradient="light-blue">
            <h1>light-blue</h1>
          </k-subheader>
          <hr>
          <k-header blue>
          </k-header>
          <k-subheader gradient="blue">
            <h1>blue</h1>
          </k-subheader>
          <hr>
          <k-header purple>purple header</k-header>
          <k-subheader gradient="purple">
            <h1>purple</h1>
          </k-subheader>
          <hr>
          <k-header purple>
            <div class="k-header-row">
              <div class="k-header-section-start">
                <a style="font-size: 16px; line-height: 12px; margin: 0; color: #fff">Cruitment</a>
                <nav class="k-header-nav" aria-label="Main Menu" role="navigation">
                  <h2 class="k-header-nav--name">Main Menu</h2>
                  <ul class="k-header-nav-list" role="menubar">
                    <li class="k-header-nav-list--item" role="menuitem">
                      <a class="k-header-nav--link k-header-nav-link--active" href="#">Home</a>
                    </li>
                    <li class="k-header-nav-list--item" role="menuitem">
                      <a class="k-header-nav--link" href="#">Contacts</a>
                    </li>
                    <li class="k-header-nav-list--item" role="menuitem"><a class="k-header-nav--link"
                                                                           href="#">Demo</a>
                    </li>
                  </ul>
                </nav>
              </div>
              <div class="k-header-section-end">
                <a href="#" class="k-header-action__link">+ New item</a>
                <a href="#" class="k-header-action__link router-link-exact-active">+ New item</a>
                <k-drop-menu placement="bottom-start">
                  <button class="k-header-action--item" data-menu-trigger>
                    <div class="k-header-action-item-title">Name</div>
                    <k-avatar name="Profile" src="https://i.pravatar.cc/40" small/>
                    <k-icon name="down-chevron" class="k-header-action-item--down"/>
                  </button>
                  <k-drop-menu-content title="Name">
                    <k-drop-menu-item><span class="k-drop-menu-item-content k-drop-menu-item-content--clickable">Item one</span>
                    </k-drop-menu-item>
                    <k-drop-menu-item><span
                      class="k-drop-menu-item-content k-drop-menu-item-content--clickable k-drop-menu-item-content--active">Second Acitve</span>
                    </k-drop-menu-item>
                    <k-drop-menu-item><span
                      class="k-drop-menu-item-content k-drop-menu-item-content--clickable">333</span>
                    </k-drop-menu-item>
                  </k-drop-menu-content>
                </k-drop-menu>
              </div>
            </div>
          </k-header>
          <k-subheader gradient="light-purple">
            <h1>light-purple</h1>
          </k-subheader>
        </div>
      `,
    }),
    { info: { summary: 'Field' } },
  )
