/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
// import { action } from '@storybook/addon-actions'
// import { linkTo } from '@storybook/addon-links'

// import KButton from '../components/KButton.vue'
//
storiesOf('Button', module)
//   .add('with text', () => ({
//     components: {  KButton },
//     template: '<k-button @click="action">Hello Button</k-button>',
//     methods: { action: action('clicked') }
//   }))
//   // .add('with JSX', () => ({
//   //   components: {  KButton },
//   //   render() {
//   //     return <k-button onClick={this.action}>With JSX</k-button>;
//   //   },
//   //   methods: { action: linkTo('Button', 'with some emoji') }
//   // }))
//   .add('with some emoji', () => ({
//     components: {  KButton },
//     template: '<k-button @click="action">😀 😎 👍 💯</k-button>',
//     methods: { action: action('clicked') }
//   }))

// import { configure } from '@storybook/vue';
//
// const req = require.context('./components', true, /.stories\.js$/)
//
// function loadStories() {
//     // console.log(req.keys())
//     req.keys().forEach(req)
// }
//
// configure(loadStories, module);
