// eslint-disable-next-line no-undef
export default function (string) {
  if (!string) return ''
  string = string.toString()
  return string.charAt(0).toUpperCase() + string.slice(1)
}
