import { create } from '@storybook/theming'

export default create({
  base: 'light',

  // colorPrimary: 'red',
  colorSecondary: '#4b6af4',

  // UI
  appBg: '#fdf4ec',
  // appContentBg: variables.graylighter1,
  // appBorderColor: variables.gray,
  // appBorderRadius: 0,
  //
  // Typography
  fontBase: `'Roboto', arial, sans-serif`,
  // fontCode: 'monospace',

  // Text colors
  textColor: '#111',
  // textInverseColor: 'rgba(255,255,255,0.9)',

  // Toolbar default and active colors
  // barTextColor: 'silver',
  // barSelectedColor: variables.blue,
  // barBg: 'hotpink',

  // Form colors
  // inputBg: 'white',
  // inputBorder: 'silver',
  // inputTextColor: variables.blacklighter1,
  // inputBorderRadius: 4,

  brandTitle: 'KSMG ui storybook',
  brandUrl: 'https://ksmg.se',
  brandImage: `data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' x='0' y='0' viewBox='30 30 140 39'%3E%3Cg%3E%3Cg id='logo-K' style='left: 0px; top: 0px;' transform='translate(0 0)'%3E%3Cpath d='M69.1,69.1H32.6V32.6h36.5V69.1z M36.5,65.2h28.7V36.5H36.5V65.2z'%3E%3C/path%3E%3Cpath d='M53.9,60.1l-5.6-7.4l-1.4,1.7v5.7h-3.9V41.6h3.9v8.2l6.5-8.2h4.8l-7.3,8.7l7.8,9.8H53.9z'%3E%3C/path%3E%3C/g%3E%3Cg id='logo-S' style='left: 0px; top: 0px;' transform='translate(0 -0.00590932)'%3E%3Cpath d='M101.7,69.1H65.2V32.6h36.5V69.1z M69.1,65.2h28.7V36.5H69.1V65.2z'%3E%3C/path%3E%3Cpath d='M75.8,57.5l2.2-3.1c1.3,1.4,3.3,2.5,5.9,2.5c2.2,0,3.2-1,3.2-2c0-3.2-10.6-1-10.6-7.9c0-3.1,2.7-5.6,7-5.6 c2.9,0,5.4,0.9,7.2,2.6l-2.2,2.9c-1.5-1.4-3.5-2-5.4-2c-1.7,0-2.6,0.7-2.6,1.8c0,2.9,10.6,0.9,10.6,7.8c0,3.4-2.4,5.9-7.4,5.9 C80.1,60.4,77.5,59.2,75.8,57.5z'%3E%3C/path%3E%3C/g%3E%3Cg id='logo-M' style='left: 0px; top: 0px;' transform='translate(0 -0.00590932)'%3E%3Cpath d='M134.3,69.1H97.8V32.6h36.5V69.1z M101.7,65.2h28.7V36.5h-28.7V65.2z'%3E%3C/path%3E%3Cpath d='M122.1,60.1V46.8l-5.2,13.3h-1.7L110,46.8v13.3H106V41.6h5.5l4.5,11.5l4.5-11.5h5.5v18.5H122.1z'%3E%3C/path%3E%3C/g%3E%3Cg id='logo-G' style='left: 0px; top: 0px;' transform='translate(0 -0.0474186)'%3E%3Cpath d='M166.9,69.1h-36.5V32.6h36.5V69.1z M134.3,65.2H163V36.5h-28.7V65.2z'%3E%3C/path%3E%3Cpath d='M139.8,50.9c0-5.8,4.4-9.6,9.9-9.6c3.8,0,6.2,1.9,7.6,4.1l-3.2,1.8c-0.9-1.3-2.4-2.4-4.4-2.4 c-3.4,0-5.8,2.6-5.8,6.1c0,3.5,2.4,6.1,5.8,6.1c1.6,0,3.2-0.7,3.9-1.4v-2.2h-4.9v-3.4h8.8V57c-1.9,2.1-4.5,3.5-7.8,3.5 C144.2,60.4,139.8,56.7,139.8,50.9z'%3E%3C/path%3E%3C/g%3E%3C/g%3E%3C/svg%3E`,
})
