import { addParameters, configure, addDecorator } from '@storybook/vue'
import { withInfo, setDefaults } from 'storybook-addon-vue-info'
import theme from './theme'
import '../../src/styles/index.scss'

setDefaults({
  header: false,
})

addDecorator(withInfo)


addParameters(
  {
    options: {
      theme: theme,
    },
  },
)

const req = require.context('../../src/stories/', true, /.stories\.js$/)

function loadStories() {
  req.keys().forEach(filename => req(filename))
}

configure(loadStories, module)
