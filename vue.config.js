const path = require('path')

module.exports = {
  chainWebpack: (config) => {
    config.module
      .rule('storysource')
      .test(/\.stories\.jsx?$/)
      .pre()
      .use('storysource')
      .loader(require.resolve('@storybook/addon-storysource/loader'))
      .options({ parser: 'typescript' })

    const svgRule = config.module.rule('svg');
    svgRule.uses.clear();
    svgRule
      .use('babel-loader')
      .loader('babel-loader')
      .end()
      .use('vue-svg-loader')
      .loader('vue-svg-loader');
    /*    config.module
          .rule('svg-sprite')
          .use('svgo-loader')
          .loader('svgo-loader')*/

    // config.module
    //   .rule('vue')
    //   .use('vue-svg-inline-loader')
    //   .loader('vue-svg-inline-loader')
    //   .options({
    //     sprite: {
    //       keyword: 'k-svg-sprite',
    //       strict: true,
    //     },
    //     removeAttributes: ['alt', 'src', 'svg-inline', 'svg-sprite'],
    //   })

  },

  pluginOptions: {
    lintStyleOnBuild: true,
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: [path.resolve(__dirname, './src/styles/index.scss')],
    },

    // svgSprite: {
    //   /* The directory containing your SVG files. */
    //   dir: 'src/assets/icons',
    //   /* The reqex that will be used for the Webpack rule. */
    //   test: /\.(svg)(\?.*)?$/,
    //   /* @see https://github.com/kisenka/svg-sprite-loader#configuration */
    //   loaderOptions: {
    //     extract: true,
    //     spriteFilename: 'img/icons/k-icons_sprite.svg', // or 'img/icons.svg' if filenameHashing == false
    //   },
    //   /* @see https://github.com/kisenka/svg-sprite-loader#configuration */
    //   pluginOptions: {
    //     plainSprite: false,
    //   },
    // },

    storybook: {
      allowedPlugins: [
        'vue-svg-loader'
        // 'svg-sprite',
        // 'svg-sprite-loader',
        // 'vue-svg-inline-loader',
      ],
    },
  },
}
